\documentclass[12pt]{article}

\usepackage{graphicx}
\graphicspath{ {./figures/} }
\usepackage{caption}
\usepackage{subcaption}

\usepackage{amsmath}
\usepackage[margin=1in]{geometry}
\usepackage{textcomp}
\renewcommand{\arraystretch}{2}

\title{Introduction to Fourier Optics}
\author{Huy Le}
\date{January 2020}

\begin{document}

\maketitle
This is a personal study of \cite{goodman}.

\section{Maxwell's equations}
In MKS units and in the absence of free change
\begin{equation}
    \nabla\times\vec{\mathbf{E}}=-\mu\frac{\partial\vec{\mathbf{H}}}{\partial t},\;
    \nabla\times\vec{\mathbf{H}}=\epsilon\frac{\partial\vec{\mathbf{E}}}{\partial t},\;
    \nabla\cdot\epsilon\vec{\mathbf{E}}=0,\;
    \nabla\cdot\mu\vec{\mathbf{H}}=0,
\end{equation}
with $\vec{\mathbf{E}}$ and $\vec{\mathbf{H}}$ being the electric and magnetic fields, and $\mu=\mu_0$ and $\epsilon$ being the magnetic and electric permeabilities.

\section{Wave equations}
In source-free medium
\begin{equation}
    \nabla^2\vec{\mathbf{E}}=\frac{n^2}{c^2}\frac{\partial \vec{\mathbf{E}}}{\partial t}
\end{equation}
where $c=\frac{1}{\sqrt{\mu_0\epsilon_0}}$ is the speed of light, $n=\sqrt{\frac{\epsilon}{\epsilon_0}}=\frac{c}{v}$ is the refractive index.

\section{Helmholtz equation}
Monochromatic wave 
\begin{equation}
    u(P,t)=A(P)\cos(2\pi\nu t-\phi(P))=\operatorname{Re}\left[U(P)e^{-j2\pi\nu t}\right].
\end{equation}
Phasor
\begin{equation}
    U(P)=A(P)e^{j\phi(P)}
\end{equation}
satisfies 
\begin{equation}
    (\nabla^2+k^2)U=0,    
\end{equation}
where $k=\frac{2\pi}{\lambda}$ and $\lambda=v\nu$.

\section{Green's theorem}
Let $S$ be the closed surface surrounding volume $V$.
\begin{equation}
    \int_V(U\nabla^2G-G\nabla^2U)dv=\int_S\left( U\frac{\partial G}{\partial n}-G\frac{\partial U}{\partial n}\right)ds
\end{equation}

\section{Free-space Green function}
\begin{equation}
    \begin{aligned}
        (\nabla^2+k^2)G&=\delta(r),\\
        G&=\frac{e^{jkr}}{r},\\
        \frac{\partial G}{\partial n}&=\cos(\vec{n},\vec{r})\left(jk-\frac{1}{r}\right)G.
    \end{aligned}
\end{equation}

\section{Helmholtz-Kirchhoff integral theorem}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{helmholtz-kirchhoff}
\caption{Illustration of Helmholtz-Kirchhoff integral theorem.}
\end{figure}

\begin{equation}
    U(P_0)=\frac{1}{4\pi}\int_S\left[ \frac{\partial U}{\partial n}\frac{e^{jkr_{01}}}{r_{01}}-U\frac{\partial}{\partial n}\left( \frac{e^{jkr_{01}}}{r_{01}}\right) \right]ds
\end{equation}

\section{Fresnel-Kirchhoff diffraction formula}
\begin{figure}
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{kirchhoff-diffraction}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{fresnel-kirchhoff}
     \end{subfigure}
     \caption{Illustration of Fresnel-Kirchhoff diffraction.}
\end{figure}

\emph{Sommerfeld radiation condition} 
\begin{equation}
    \lim_{R\to\infty} R\left( \frac{\partial U}{\partial n}-jkU\right)=0
\end{equation}
satisfies when $U$ vanishes at least as fast as a diverging sppherical wave. Then integral on $S_2$ vanishes.
\par
\emph{Kirchhoff boundary conditions}
\begin{itemize}
    \item Across the aperture surface $\Sigma$, $U$ and its derivative are the same as they would be in the absence of the screen.
    \item $U$ and its derivative are zero behind the screen.
\end{itemize}
\par
Note that $r_{01}\gg \lambda$, Helmholtz-Kirchhoff integral becomes
\begin{equation}
    U(P_0)=\frac{1}{4\pi}\int_{\Sigma}\frac{e^{jkr_{01}}}{r_{01}}\left[ \frac{\partial U}{\partial n}-jkU\cos(\vec{n},\vec{r_{01}})\right]ds
\end{equation}
If the aperture is illuminated by a single spherical wave
\begin{equation}
    U(P_1)=\frac{Ae^{jkr_{21}}}{r_{21}},
\end{equation}
we have \emph{Fresnel-Kirchhoff diffraction formula}
\begin{equation}
    U(P_0)=\frac{A}{j\lambda}\int_{\Sigma}\frac{e^{jk(r_{21}+r_{01})}}{r_{21}r_{01}}\frac{\cos(\vec{n},\vec{r_{01}})-\cos(\vec{n},\vec{r_{21}})}{2}ds=\int_{\Sigma}U'(P_1)\frac{e^{jkr_{01}}}{r_{01}}ds
\end{equation}
with
\begin{equation}
    U'(P_1)=\frac{1}{j\lambda}\frac{Ae^{jkr_{21}}}{r_{21}}\frac{\cos(\vec{n},\vec{r_{01}})-\cos(\vec{n},\vec{r_{21}})}{2}
\end{equation} 

that can be interpreted as superposition of many secondary sources $U'(P_1)$.

\section{Rayleigh-Sommerfeld diffraction}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{rayleigh-sommerfeld}
\caption{Illustration of Rayleigh-Sommerfeld diffraction.}
\end{figure}

Choose
\begin{equation}
    G_{-}(P_1)=\frac{e^{jkr_{01}}}{r_{01}}-\frac{e^{jk\tilde{r}_{01}}}{\tilde{r}_{01}},
\end{equation}
then on the aperture surface
\begin{equation}
    \frac{\partial G_{-}}{\partial n}=2\frac{\partial G}{\partial n},
\end{equation}
we have the \emph{first Rayleigh-Sommerfeld solution}
\begin{equation}
\begin{aligned}
    U_I(P_0)&=\frac{-1}{2\pi}\int_{\Sigma}U\frac{\partial G}{\partial n}ds=\frac{1}{j\lambda}\int_{\Sigma}U(P_1)\frac{e^{jkr_{01}}}{r_{01}}\cos(\vec{n},\vec{r_{01}})ds\\
    &=\frac{A}{j\lambda}\int_{\Sigma}\frac{e^{jk(r_{21}+r_{01})}}{r_{21}r_{01}}\cos(\vec{n},\vec{r_{01}})ds
\end{aligned}
\end{equation}
Choose
\begin{equation}
    G_{+}(P_1)=\frac{e^{jkr_{01}}}{r_{01}}+\frac{e^{jk\tilde{r}_{01}}}{\tilde{r}_{01}},
\end{equation}
then on the aperture surface
\begin{equation}
    G_{+}=2G,
\end{equation}
we have the \emph{second Rayleigh-Sommerfeld solution}
\begin{equation}
\begin{aligned}
    U_{II}(P_0)&=\frac{1}{2\pi}\int_{\Sigma}\frac{\partial U}{\partial n}Gds=\frac{-1}{j\lambda}\int_{\Sigma}U(P_1)\frac{e^{jkr_{01}}}{r_{01}}\cos(\vec{n},\vec{r_{01}})ds\\
    &=\frac{-A}{j\lambda}\int_{\Sigma}\frac{e^{jk(r_{21}+r_{01})}}{r_{21}r_{01}}\cos(\vec{n},\vec{r_{21}})ds
\end{aligned}
\end{equation}
Note that Kirchhoff solution is the average of the two Rayleigh-Sommerfeld solutions. The three solutions are the same if the aperture diameter is much greater than the wavelength or only small angles involve, i.e. when the observation plane is far from the aperture.

\section{Huygens-Fresnel principle}
Predicted by the first Rayleigh-Sommerfeld solution
\begin{equation}
    U_I(P_0)=\frac{1}{j\lambda}\int_{\Sigma}U(P_1)\frac{e^{jkr_{01}}}{r_{01}}\cos\theta ds=\int_{\Sigma}h(P_0,P_1)U(P_1)ds,
\end{equation}
with impulse response
\begin{equation}
    h(P_0,P_1)=\frac{1}{j\lambda}\frac{e^{jkr_{01}}}{r_{01}}\cos\theta ds,
\end{equation}
as superposition of secondary sources, which
\begin{itemize}
    \item has complex amplitude proportional to the incident wave on the aperture $U(P_1)$ and inverse proportional to wavelength $\lambda$,
    \item leads the phase of the incident wave by 90\textdegree indicated by factor $1/j$, and
    \item has a directivity $\cos\theta$.
\end{itemize}

\section{Angular spectrum}
A monochromatic wave incident on the plane $z=0$
\begin{equation}
    U(x,y,0)=\int A(f_X,f_Y,0)e^{j2\pi(f_Xx+f_Yy)}df_Xdf_Y,
\end{equation}
is supposition  of plane waves $e^{j2\pi(f_Xx+f_Yy)}$ with amplitudes
\begin{equation}
    A(f_X,f_Y,0)=\int U(x,y,0)e^{-j2\pi(f_Xx+f_Yy)}dxdy.
\end{equation}
A general plane wave $e^{j(\vec{k}\cdot\vec{r}-2\pi\nu t)}$ has $\vec{k}=\frac{2\pi}{\lambda}(\alpha\hat{x}+\beta\hat{y}+\gamma\hat{z})$ and $\vec{r}=x\hat{x}+y\hat{y}+z\hat{z}$. Then $e^{j2\pi(f_Xx+f_Yy)}$ is a plane wave with directional cosines
\begin{equation}
    \alpha=\lambda f_X,\;\beta=\lambda f_Y,\;\gamma=\sqrt{1-(\lambda f_X)^2-(\lambda f_Y)^2}.
\end{equation}
Define \emph{angular spectrum}
\begin{equation}
    A\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda},0\right)=\int U(x,y,0)e^{-j2\pi\left(\frac{\alpha}{\lambda}x+\frac{\beta}{\lambda}y\right)}dxdy.
\end{equation}

\section{Propagation of angular spectrum}
\begin{equation}
    U(x,y,z)=\int A\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda},z\right)e^{j2\pi\left(\frac{\alpha}{\lambda}x+\frac{\beta}{\lambda}y\right)}d\frac{\alpha}{\lambda}d\frac{\beta}{\lambda}
\end{equation}
satisfies Helmholtz equation, so the angular spectrum 
\begin{equation}
    A\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda},z\right)=\int U(x,y,z)e^{-j2\pi\left(\frac{\alpha}{\lambda}x+\frac{\beta}{\lambda}y\right)}dxdy
\end{equation}
satisfies
\begin{equation}
    \frac{d^2}{dz^2}A\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda},z\right)+\left(\frac{2\pi}{\lambda}\right)^2(1-\alpha^2-\beta^2)A\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda},z\right)=0,
\end{equation}
which has solution
\begin{equation}
    A\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda},z\right)=A\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda},0\right)e^{j\frac{2\pi}{\lambda}\sqrt{1-\alpha^2-\beta^2}},
\end{equation}
or
\begin{equation}
    A(f_X,f_Y,z)=A(f_X,f_Y,0)H(f_X,f_Y),
\end{equation}
in which the transfer function of propagation 
\begin{equation}
    H(f_X,f_Y)=e^{j2\pi\frac{z}{\lambda}\sqrt{1-(\lambda f_X)^2-(\lambda f_Y)^2}}.
\end{equation}

\section{Effect of diffracting aperture on angular spectrum}
Suppose aperture located on $z=0$. Relation between the transmitted and incident fields is
\begin{equation}
    U_t(x,y,0)=U_i(x,y,0)t_A(x,y,0),
\end{equation}
or
\begin{equation}
    A_t\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda}\right)=A_i\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda}\right)\otimes T\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda}\right)
\end{equation}
If the incident is a unit amplitude plane wave
\begin{equation}
    A_i\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda}\right)=\delta\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda}\right),
\end{equation}
then 
\begin{equation}
    A_t\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda}\right)=T\left(\frac{\alpha}{\lambda},\frac{\beta}{\lambda}\right).
\end{equation}

\section{Fresnel diffraction}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{diffraction}
\caption{Setting for Fresnel and Fraunhofer diffractions.}
\end{figure}

With 
\begin{equation}
    \cos\theta=\frac{z}{r_{01}},
\end{equation}
and
\begin{equation}
\begin{aligned}
    r_{01}&=\sqrt{z^2+(x-\xi)^2+(y-\eta)^2},\\
    &=z\sqrt{1+\left(\frac{x-\xi}{z}\right)^2+\left(\frac{y-\eta}{z}\right)^2},\\
    &\approx z\left[1+\frac{1}{2}\left(\frac{x-\xi}{z}\right)^2+\frac{1}{2}\left(\frac{y-\eta}{z}\right)^2\right],
\end{aligned}
\end{equation}
Huygens-Fresnel principle, i.e. Rayleight-Sommerfeld first solution, becomes 
\begin{equation}
\begin{aligned}
    U(x,y)&=\frac{1}{j\lambda}\int U(P_1)\frac{e^{jkr_{01}}}{r_{01}}\cos\theta ds,\\
    &=\frac{e^{jkz}}{j\lambda z}\int U(\xi,\eta)e^{j\frac{k}{2z}\left[(x-\xi)^2+(y-\eta)^2\right]}d\xi d\eta,\\
    &=\int U(\xi,\eta)h(x-\xi,y-\eta)d\xi d\eta,\\
    &=\frac{e^{jkz}}{j\lambda z}e^{j\frac{k}{2z}(x^2+y^2)}\int U(\xi,\eta)e^{j\frac{k}{2z}(\xi^2+\eta^2)}e^{-j\frac{2\pi}{\lambda z}(x\xi+y\eta)}d\xi d\eta,
\end{aligned}
\end{equation}
in which the last equation is the Fourier transform of the product of the field just right of the aperture and a quadratic phase exponential.
\par
The third equation expresses Fresnel diffraction as a convolution with the kernel
\begin{equation}
    h(x,y)=\frac{e^{jkz}}{j\lambda z}e^{j\frac{\pi}{\lambda z}(x^2+y^2)},
\end{equation}
whose Fourier transform gives the transfer function for Fresnel diffraction
\begin{equation}
    H(f_X,f_Y)=\mathcal{F}[h(x,y)]=e^{jkz}e^{-j\pi \lambda z(f_X^2+f_Y^2)},
\end{equation}
which, beside a constant phase delay $e^{jkz}$ suffered by all plane waves traveling a distance $z$, is an approximation of the propagation transfer function
\begin{equation}
    H(f_X,f_Y)=e^{j2\pi\frac{z}{\lambda}\sqrt{1-(\lambda f_X)^2-(\lambda f_Y)^2}},
\end{equation}
when 
\begin{equation}
    \sqrt{1-(\lambda f_X)^2-(\lambda f_Y)^2}\approx 1-\frac{(\lambda f_X)^2}{2}-\frac{(\lambda f_Y)^2}{2},
\end{equation}
which is valid under the paraxial approximation (i.e. small angle of diffraction) $|\lambda f_X|\ll 1$ and $|\lambda f_Y|\ll 1$.

\section{Fraunhofer diffraction}
When a stronger, i.e. far field, approximation is satisfied
\begin{equation}
    z\gg\frac{k(\xi^2+\eta^2)_{max}}{2},
\end{equation}
Fresnel diffraction becomes directly the Fourier transform of the aperture distribution (when the illumination source is a unit-amplitude normal-incident monochromatic plane wave)
\begin{equation}
    U(x,y)=\frac{e^{jkz}}{j\lambda z}e^{j\frac{k}{2z}(x^2+y^2)}\int U(\xi,\eta)e^{-j\frac{2\pi}{\lambda z}(x\xi+y\eta)}d\xi d\eta,
\end{equation}
evaluated at $f_X=\frac{x}{\lambda z},\;f_Y=\frac{y}{\lambda z}$.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{rectangle}
\caption{Fraunhofer diffraction of a rectangle.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{single-slit}
\caption{Fraunhofer diffraction of a single slit.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{double-slits}
\caption{Fraunhofer diffraction of double slits.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{circle}
\caption{Fraunhofer diffraction of a circle.}
\end{figure}

\section{Thin lens}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{lens0}
\caption{Thin lens: front view (left) and side view (right).}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{lens1}
\caption{Thin lens thickness.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{lens2}
\caption{Transmittance function of a thin lens.}
\end{figure}

Effect of a thin lens, whose thickness is $\triangle(x,y)$ and refractive index is $n$, is a phase delay
\begin{equation}
    \phi(x,y)=kn\triangle(x,y)+k[\triangle_0-\triangle(x,y)],
\end{equation}
characterized by the transmittance function
\begin{equation}
    t_l(x,y)=e^{jk\triangle_0}e^{jk(n-1)\triangle(x,y)}
\end{equation}
The thickness
\begin{equation}
    \triangle(x,y)=\triangle_0-R_1\left(1-\sqrt{1-\frac{x^2+y^2}{R_1^2}}\right)+R_2\left(1-\sqrt{1-\frac{x^2+y^2}{R_2^2}}\right),
\end{equation}
which under the paraxial approximation 
\begin{equation}
    \sqrt{1-\frac{x^2+y^2}{R_i^2}}\approx 1-\frac{x^2+y^2}{2R_i^2},
\end{equation}
becomes
\begin{equation}
    \triangle(x,y)=\triangle_0-\frac{x^2+y^2}{2}\left(\frac{1}{R_1}-\frac{1}{R_2}\right).
\end{equation}
The lens transmittance function is now
\begin{equation}
    t(x,y)=e^{-j\frac{k}{2f}(x^2+y^2)},
\end{equation}
in which the constant phase factor $e^{jkn\triangle_0}$ has been dropped and the focal length $f$ is defined
\begin{equation}
    \frac{1}{f}=(n-1)\left(\frac{1}{R_1}-\frac{1}{R_2}\right).
\end{equation}

\section{Fourier transform properties of lenses}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{lens3}
\caption{Fourier transform properties of a thin lens.}
\end{figure}

\subsection{Input placed against the lens}
Define the transmittance function of the input $t_A(x,y)$, which is uniformly illuminated by a normal-incident monochromatic plane wave of amplitude $A$. The field incident on the lens (i.e. behind the input) is
\begin{equation}
    U_l(x,y)=At_A(x,y).
\end{equation}
The lens is associated with a pupil function 
\begin{equation}
    P(x,y)=\begin{cases}
    1 \mbox{ inside the lens aperture},\\
    0 \mbox{ otherwise}.
    \end{cases}
\end{equation}
The field behind the lens is
\begin{equation}
    U'_l(x,y)=U_l(x,y)P(x,y)e^{-j\frac{k}{2f}(x^2+y^2)}.
\end{equation}
The field on the back focal plane of the lens is found by the Fresnel diffraction formula (dropping the constant phase factor)
\begin{equation}
\begin{aligned}
    U_f(u,v)&=\frac{e^{j\frac{k}{2f}(u^2+v^2)}}{j\lambda f}\int U'_l(x,y)e^{j\frac{k}{2f}(x^2+y^2)}e^{-j\frac{2\pi}{\lambda f}(xu+yv)}dxdy,\\
    &=\frac{e^{j\frac{k}{2f}(u^2+v^2)}}{j\lambda f}\int U_l(x,y)P(x,y)e^{-j\frac{2\pi}{\lambda f}(xu+yv)}dxdy,
\end{aligned}
\end{equation}
which, if $P(x,y)$ is ignored, becomes the Fraunhofer diffraction of the field incident on the lens at frequencies $f_X=\frac{u}{\lambda f}$ and $f_Y=\frac{v}{\lambda f}$, even though the distance to the observation plane is equal to the focal length of the lens rather than satisfying the Fraunhofer far field approximation. 

\subsection{Input placed in front of the lens}
Define the Fourier spectra of the field transmitted by the input and the field incident on the lens
\begin{equation}
    F_0(f_X,f_Y)=\mathcal{F}(At_A),\;F_l(f_X,f_Y)=\mathcal{F}(U_l).
\end{equation}
Under paraxial approximation, they are related by propagation transfer function over distance $d$
\begin{equation}
    F_l(f_X,f_Y)=F_0(f_X,f_Y)e^{-j\pi\lambda d(f_X^2+f_Y^2)},
\end{equation}
in which the constant phase factor has been dropped.
\par
Apply the lens transmittance function and the Fresnel diffraction formula and neglect $P$ to arrive at
\begin{equation}
\begin{aligned}
    U_f(u,v)&=\frac{e^{j\frac{k}{2f}(u^2+v^2)}}{j\lambda f}F_l\left(\frac{u}{\lambda f},\frac{v}{\lambda f}\right),\\
    &=\frac{e^{j\frac{k}{2f}\left(1-\frac{d}{f}\right)(u^2+v^2)}}{j\lambda f}F_0\left(\frac{u}{\lambda f},\frac{v}{\lambda f}\right),\\
    &=\frac{Ae^{j\frac{k}{2f}\left(1-\frac{d}{f}\right)(u^2+v^2)}}{j\lambda f}\int t_A(\xi,\eta)e^{-j\frac{2\pi}{\lambda f}(\xi u+\eta v)}d\xi d\eta.
\end{aligned}
\end{equation}
When the input is placed exactly at the front focal distance of the lens, $d=f$, the field on the back focal plane is the exact Fourier transform of the input transmittance function.
\par
The finite extent of the lens pupil is accounted for using geometrical optics approximation, satisfied when the input is well within the Fresnel diffraction zone of the lens, and by projecting the lens aperture back to the input plane 
\begin{equation}
    U_f(u,v)=\frac{Ae^{j\frac{k}{2f}\left(1-\frac{d}{f}\right)(u^2+v^2)}}{j\lambda f}\int t_A(\xi,\eta)P\left(\xi+\frac{d}{f}u,\eta+\frac{d}{f}v\right)e^{-j\frac{2\pi}{\lambda f}(\xi u+\eta v)}d\xi d\eta.
\end{equation}

\subsection{Input placed behind the lens}
Now the lens is illuminated directly by a normal-incident monochromatic plane wave of uniform amplitude $A$. The field behind the lens hence is a spherical wave converging towards to back focal point. The amplitude of this field at the input is $\frac{Af}{d}$ for energy conservation reason. The illumination area on the input is the projection of the lens pupil $P\left(\xi\frac{f}{d},\eta\frac{f}{d}\right)$. Altogether, the field transmitted by the input is
\begin{equation}
    U_0(\xi,\eta)=\frac{Af}{d}P\left(\xi\frac{f}{d},\eta\frac{f}{d}\right)e^{-j\frac{k}{2d}(\xi^2+\eta^2)}t_A(\xi,\eta).
\end{equation}
The quadratic phase exponential in the above equation will exactly cancel when applying Fresnel diffraction formula to find the field on the back focal plane
\begin{equation}
    U_f(u,v)=\frac{Ae^{j\frac{k}{2d}(u^2+v^2)}}{j\lambda d}\frac{f}{d}\int t_A(\xi,\eta)P\left(\xi\frac{f}{d},\eta\frac{f}{d}\right)e^{-j\frac{2\pi}{\lambda d}(\xi u+\eta v)}d\xi d\eta.
\end{equation}

\section{Image formation}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{image-formation}
\caption{Image formation.}
\end{figure}

Due to the linearity of the wave propagation phenomenon, the image field $U_i(u,v)$ at a distance $z_2$ behind the lens is related to the field $U_0(\xi,\eta)$ at distance $z_1$ in front of the lens by
\begin{equation}
    U_i(u,v)=\int h(u,v,\xi,\eta)U_0(\xi,\eta)d\xi d\eta.
\end{equation}
For the image to be as similar to the object as possible, the impulse response should closely approximate a Dirac delta function
\begin{equation}
    h(u,v,\xi,\eta)\approx K\delta(u\pm M\xi,v\pm M\eta),
\end{equation}
where $K$ is a complex constant, $M$ is the magnification, and the plus and minus signs allow for the absence and presence of image inversion. 
\par
To find the impulse response, let the object be a delta function at $(\xi,\eta)$, i.e. the incident on the lens is a spherical wave diverging from $(\xi,\eta)$. The paraxial approximation to that wave is
\begin{equation}
    U_l(x,y)=\frac{1}{j\lambda z_1}e^{j\frac{k}{2z_1}\left[(x-\xi)^2+(y-\eta)^2\right]}.
\end{equation}
After passing through the lens, the field becomes
\begin{equation}
    U'_l(x,y)=U_l(x,y)P(x,y)e^{-j\frac{k}{2f}(x^2+y^2)}.
\end{equation}
Use Fresnel diffraction formula to account for propagation over distance $z_2$ behind the lens
\begin{equation}
\begin{aligned}
    h(u,v,\xi,\eta)&=\frac{1}{j\lambda z_2}\int U'_l(x,y)e^{j\frac{k}{2z_2}\left[(u-x)^2+(v-y)^2\right]},\\
    &=\frac{1}{\lambda^2z_1z_2}e^{j\frac{k}{2z_2}(u^2+v^2)}e^{j\frac{k}{2z_1}(\xi^2+\eta^2)}\\
    &\int P(x,y)e^{j\frac{k}{2}\left(\frac{1}{z_1}+\frac{1}{z_2}-\frac{1}{f}\right)(x^2+y^2)}e^{-jk\left[\left(\frac{\xi}{z_1}+\frac{u}{z_2}\right)x+\left(\frac{\eta}{z_1}+\frac{v}{z_2}\right)y\right]}dxdy.
\end{aligned}
\end{equation}
If the lens law of geometrical optics is satisfied
\begin{equation}
    \frac{1}{z_1}+\frac{1}{z_2}-\frac{1}{f}=0,
\end{equation}
and other approximations apply such that the two quadratic phase factors preceding the integral can be neglected, the impulse response becomes
\begin{equation}
\begin{aligned}
    h(u,v,\xi,\eta)&\approx \frac{1}{\lambda^2z_1z_2}\int P(x,y)e^{-jk\left[\left(\frac{\xi}{z_1}+\frac{u}{z_2}\right)x+\left(\frac{\eta}{z_1}+\frac{v}{z_2}\right)y\right]}dxdy,\\
    &=\frac{1}{\lambda^2z_1z_2}\int P(x,y)e^{-j\frac{2\pi}{\lambda z_2}[(u-M\xi)x+(v-M\eta)y]}dxdy,
\end{aligned}
\end{equation}
where $M=-\frac{z_2}{z_1}$ is the magnification. This is, up to a factor of $\frac{1}{\lambda z_1}$, the Fraunhofer diffraction of the lens aperture, which should not be surprising because by choosing $z_2$ that satisfies the lens law, we have chosen to examine the plane toward which the spherical wave leaving the lens converges.
\par
When $\lambda\to0$, we get the result of geometrical optics
\begin{equation}
    h(u,v,\xi,\eta)\to\frac{1}{|M|}\delta\left(\xi-\frac{u}{M},\eta-\frac{v}{M}\right),
\end{equation}
and the image is simply an inverted and magnified replication of the object
\begin{equation}
    U_i(u,v)=\frac{1}{|M|}U_0\left(\frac{u}{M},\frac{v}{M}\right).
\end{equation}
\par
Normalize the object coordinates to remove inversion and magnification
\begin{equation}
    \tilde{\xi}=M\xi,\;\tilde{\eta}=M\eta,\;\tilde{x}=\frac{x}{\lambda z_2},\;\tilde{y}=\frac{y}{\lambda z_2},\;\tilde{h}=\frac{h}{|M|},
\end{equation}
then
\begin{equation}
    U_i(u,v)=\tilde{h}(u,v)\otimes U_g(u,v),    
\end{equation}
where 
\begin{equation}
    U_g(u,v)=\frac{1}{|M|}U_0\left(\frac{u}{M},\frac{v}{M}\right),
\end{equation}
is the geometrical optics prediction of the image, and 
\begin{equation}
    \tilde{h}(u,v)=\int P(\lambda z_2\tilde{x},\lambda z_2\tilde{y})e^{-j2\pi(u\tilde{x}+v\tilde{y})}d\tilde{x}d\tilde{y},
\end{equation}
is the point spread function introduced by diffraction.
\par
\emph{Conclusions}
\begin{itemize}
    \item The ideal image produced by a diffraction-limited (i.e. free from aberrations) is a scaled and inverted version of the object.
    \item The effect of diffraction is to convolve (i.e. to smooth) that ideal image with the Fraunhofer diffraction pattern on the lens pupil. 
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{H}
\caption{Object pattern illuminated by a normal incident monochromatic plane wave.}
\end{figure}

\begin{figure}
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{duv09}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{duv135}
     \end{subfigure}
     \caption{Images with DUV, $\lambda=193$ nm, and $NA=0.9$ (left) and $NA=1.35$ (right).}
\end{figure}

\begin{figure}
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{uv09}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{uv135}
     \end{subfigure}
     \caption{Images with UV, $\lambda=100$ nm, and $NA=0.9$ (left) and $NA=1.35$ (right).}
\end{figure}

\begin{figure}
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{euv09}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{euv135}
     \end{subfigure}
     \caption{Images with EUV, $\lambda=13.5$ nm, and $NA=0.9$ (left) and $NA=1.35$ (right).}
\end{figure}

\section{Operator notation}
\emph{Multiplication by a quadratic phase factor}
\begin{equation}
    \mathcal{Q}[c]\{U(x\}=e^{j\frac{k}{2}cx^2}U(x).
\end{equation}
\emph{Scaling by a constant}
\begin{equation}
    \mathcal{V}[b]\{U(x)\}=|b|^{1/2}U(bx).
\end{equation}
\emph{Fourier transform}
\begin{equation}
    \mathcal{F}\{U(x)\}=\int U(x)e^{j2\pi fx}dx.
\end{equation}
\emph{Free-space propagation}
\begin{equation}
    \mathcal{R}[d]\{U(x_1)\}=\frac{1}{\sqrt{j\lambda d}}\int e^{j\frac{k}{2d}(x_2-x_1)^2}U(x_1)dx_1.
\end{equation}
With these operators
\begin{itemize}
    \item Effect of lens with focal length $f$ is $\mathcal{Q}\left[-\frac{1}{f}\right]$.
    \item Fresnel diffraction over distance $d$ is $\mathcal{R}[d]$.
    \item Paraxial approximation to a spherical wave diverging from a point source at distance $d$ is $\mathcal{Q}\left[\frac{1}{d}\right]$.
\end{itemize}

\begin{table}[h!]
\centering
\resizebox{\columnwidth}{!}{
\begin{tabular}{|c|c|c|c|c|} 
 \hline
 & $\mathcal{V}$ & $\mathcal{F}$ & $\mathcal{Q}$ & $\mathcal{R}$ \\
 \hline
 $\mathcal{V}$ & $\mathcal{V}[t_2]\mathcal{V}[t_1]=\mathcal{V}[t_2t_1]$ & $\mathcal{V}[t]\mathcal{F}=\mathcal{F}\mathcal{V}\left[\frac{1}{t}\right]$ & $\mathcal{V}[t]\mathcal{Q}[c]=\mathcal{Q}[t^2c]\mathcal{V}[t]$ & $\mathcal{V}[t]\mathcal{R}[d]=\mathcal{R}\left[\frac{d}{t^2}\right]\mathcal{V}[t]$ \\ 
 \hline
 $\mathcal{F}$ & $\mathcal{F}\mathcal{V}[t]=\mathcal{V}\left[\frac{1}{t}\right]\mathcal{F}$ & $\mathcal{F}\mathcal{F}=\mathcal{V}[-1]$ & $\mathcal{F}\mathcal{Q}[c]=\mathcal{R}\left[-\frac{c}{\lambda^2}\right]\mathcal{F}$ & $\mathcal{F}\mathcal{R}[d]=\mathcal{Q}[\lambda^2d]\mathcal{F}$ \\
 \hline
 $\mathcal{Q}$ & $\mathcal{Q}[c]\mathcal{V}[t]=\mathcal{V}[t]\mathcal{Q}\left[\frac{c}{t^2}\right]$ & $\mathcal{Q}[c]\mathcal{F}=\mathcal{F}\mathcal{R}\left[-\frac{c}{\lambda^2}\right]$ & $\mathcal{Q}[c_2]\mathcal{Q}[c_1]=\mathcal{C}[c_2+c_1]$ & \(\mathcal{Q}[c]\mathcal{R}[d]=\mathcal{R}\left[\left(d^{-1}+c\right)^{-1}\right]\)\\
 & & & & \(\cdot\mathcal{V}[1+cd]\mathcal{Q}\left[\left(c^{-1}+d\right)^{-1}\right]\)\\
 \hline
 $\mathcal{R}$ & $\mathcal{R}[d]\mathcal{V}[t]=\mathcal{V}[t]\mathcal{R}[t^2d]$ & $\mathcal{R}[d]\mathcal{F}=\mathcal{F}\mathcal{Q}[\lambda^2d]$ & \(\mathcal{R}[d]\mathcal{Q}[c]=\mathcal{Q}\left[\left(c^{-1}+d\right)^{-1}\right]\) & $\mathcal{R}[d_2]\mathcal{R}[d_1]=\mathcal{R}[d_2+d_1]$ \\
 & & & \(\cdot\mathcal{V}[(1+cd)^{-1}]\mathcal{R}\left[\left(d^{-1}+c\right)^{-1}\right]\) & \\
 \hline
\end{tabular}}
\caption{Relations between operators}
\end{table}

\section{Frequency analysis of imaging systems}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{general-model}
\caption{A generalized model.}
\end{figure}

\subsection{A generalized model}
The imaging system is said to be \emph{diffraction-limited} when a diverging spherical wave from a point source is converted by the system into a perfectly spherical wave converging to a image point. The location of the image point is related to the location of the object point by a simple scaling factor, i.e. the magnification.
\par
The entrance and exit pupils are in fact images of the same limiting aperture within the system. It is assumed that the passage of light between the entrance and the exit pupils is adequately described by geometrical optics. As a result, diffraction effects play a role only during the passage of light from the object to the entrance pupil, or alternatively and equivalently, from the exit pupil to the image. Regarding diffraction as coming from the entrance pupil, only the low-frequency components of the object amplitude transmittance are intercepted by this pupil. Here we however adopt the viewpoint that diffraction comes from the exit pupil.
\par
Again the image amplitude is the superposition
\begin{equation}
    U_i(u,v)=\int h(u,v,\xi,\eta)U_0(\xi,\eta)d\xi d\eta,
\end{equation}
of the amplitude transmitted by the object $U_0$ and the impulse response
\begin{equation}
    h(u,v,\xi,\eta)=\frac{A}{\lambda z_i}\int P(x,y)e^{-j\frac{2\pi}{\lambda z_i}[(u-M\xi)+(v-M\eta)]}dxdy.
\end{equation}
Define reduced coordinates 
\begin{equation}
    \tilde{\xi}=M\xi,\;\tilde{\eta}=M\eta,
\end{equation}
and the geometrical optics prediction
\begin{equation}
    U_g(\tilde{\xi},\tilde{\eta})=\frac{1}{|M|}U_0\left(\frac{\tilde{\xi}}{M},\frac{\tilde{\eta}}{M}\right),
\end{equation}
the image becomes a convolution
\begin{equation}
    U_i(u,v)=\int h(u-\tilde{\xi},v-\tilde{\eta})U_g(\tilde{\xi},\tilde{\eta})d\tilde{\xi}d\tilde{\eta},
\end{equation}
with
\begin{equation}
    h(u,v)=\frac{A}{\lambda z_i}\int P(x,y)e^{-j\frac{2\pi}{\lambda z_i}(ux+vy)}dxdy=\frac{A}{\lambda z_i}\mathcal{F}\{P(x,y)\}\left(\frac{u}{\lambda z_i},\frac{v}{\lambda z_i}\right).
\end{equation}

\subsection{Polychromatic illumination}
When the illumination is polychromatic but narrowband, i.e. occupying a bandwidth that is small compared to the center frequency, the phasor depends on both time and space
\begin{equation}
    u(P,t)=U(P,t)e^{j2\pi\bar{\nu}t},
\end{equation}
and the impulse response does not vary appreciably such that
\begin{equation}
    U_i(u,v,t)=\int h(u-\tilde{\xi},v-\tilde{\eta})U_g(\tilde{\xi},\tilde{\eta},t)d\tilde{\xi}d\tilde{\eta}.
\end{equation}
The image intensity is the time average of the instantaneous intensity
\begin{equation}
\begin{aligned}
    I_i(u,v)&=\langle|U_i(u,v,t)|^2\rangle,\\
    &=\int d\tilde{\xi}_1d\tilde{\eta}_1\int d\tilde{\xi}_2d\tilde{\eta}_2\, h(u-\tilde{\xi}_1,v-\tilde{\eta}_1)h^*(u-\tilde{\xi}_2,v-\tilde{\eta}_2)\\
    &\times \langle U_g(\tilde{\xi}_1,\tilde{\eta}_1,t)U^*_g(\tilde{\xi}_2,\tilde{\eta}_2,t) \rangle,\\
    &=\int d\tilde{\xi}_1d\tilde{\eta}_1\int d\tilde{\xi}_2d\tilde{\eta}_2\, h(u-\tilde{\xi}_1,v-\tilde{\eta}_1)h^*(u-\tilde{\xi}_2,v-\tilde{\eta}_2)J_g(\tilde{\xi}_1,\tilde{\eta}_1;\tilde{\xi}_2,\tilde{\eta}_2),
\end{aligned}
\end{equation}
where 
\begin{equation}
    J_g(\tilde{\xi}_1,\tilde{\eta}_1;\tilde{\xi}_2,\tilde{\eta}_2)=\langle U_g(\tilde{\xi}_1,\tilde{\eta}_1,t)U^*_g(\tilde{\xi}_2,\tilde{\eta}_2,t) \rangle
\end{equation}
is the \emph{mutual intensity} and is a measure of the \emph{spatial coherence} of the illumination source. 
\subsubsection{Coherent case}
When the object illumination is coherent, so are the various impulse responses, and therefore must be added in complex amplitude basis. As a result, a coherent imaging system is linear in complex amplitude.
\par
In the coherent case, the time-varying phasors across the object plane differ by only a complex phase
\begin{equation}
    U_g(\tilde{\xi}_{1,2},\tilde{\eta}_{1,2},t)=U(\tilde{\xi}_{1,2},\tilde{\eta}_{1,2})\frac{U_g(0,0,t)}{\sqrt{\langle |U_g(0,0,t)|^2\rangle}},
\end{equation}
relative to the reference phase at the origin. Consequently
\begin{equation}
    J_g(\tilde{\xi}_1,\tilde{\eta}_1;\tilde{\xi}_2,\tilde{\eta}_2)= U_g(\tilde{\xi}_1,\tilde{\eta}_1)U^*_g(\tilde{\xi}_2,\tilde{\eta}_2),
\end{equation}
and 
\begin{equation}
    I_i(u,v)=\left| \int h(u-\tilde{\xi},v-\tilde{\eta})U_g(\tilde{\xi},\tilde{\eta})d\tilde{\xi}d\tilde{\eta}\right|^2.
\end{equation}
Define the time-invariant phasor $U_i$ in the image plane relative to the corresponding phasor at the origin
\begin{equation}
    U_i(u,v)=\int h(u-\tilde{\xi},v-\tilde{\eta})U_g(\tilde{\xi},\tilde{\eta})d\tilde{\xi}d\tilde{\eta},
\end{equation}
which is the same result obtained from the monochromatic case.
\par
In the frequency domain
\begin{equation}
    G_i(f_X,f_Y)=H(f_X,f_Y)G_g(f_X,f_Y),
\end{equation}
where the \emph{amplitude transfer function} (ATF)
\begin{equation}
\begin{aligned}
    H(f_X,f_Y)&=\mathcal{F}\{h(u,v)\}=\mathcal{F}\left\{\frac{A}{\lambda z_i}\mathcal{F}\{P(x,y)\}\left(\frac{u}{\lambda z_i},\frac{v}{\lambda z_i}\right)\right\},\\
    &=A\lambda z_i P(-\lambda z_if_X,-\lambda z_if_Y),\\
    &\approx P(\lambda z_if_X,\lambda z_if_Y).
\end{aligned}
\end{equation}
This means the pupil aperture acts as a bandpass filter limiting the range of Fourier components passed by the system.

\subsubsection{Incoherent case}
When the object illumination is incoherent, the various impulse responses in the image plane are uncorrelated and must therefore be added on a power or intensity basis. An incoherent imaging system is linear in intensity and the impulse response of such system is the squared magnitude of the amplitude response. In this case
\begin{equation}
    J_g(\tilde{\xi}_1,\tilde{\eta}_1;\tilde{\xi}_2,\tilde{\eta}_2)=\kappa I_g(\tilde{\xi}_1,\tilde{\eta}_1)\delta(\tilde{\xi}_1-\tilde{\xi}_2,\tilde{\eta}_1-\tilde{\eta}_2),
\end{equation}
and
\begin{equation}
    I_i(u,v)=\kappa \int\left| h(u-\tilde{\xi},v-\tilde{\eta})\right|^2 I_g(\tilde{\xi},\tilde{\eta})d\tilde{\xi}d\tilde{\eta},
\end{equation}
where $\kappa$ is a real constant.
\par
Define the normalized frequency spectra of $I_i$ and $I_g$
\begin{equation}
    \mathcal{G}_{g,i}(f_X,f_Y)=\frac{\int I_{g,i}(u,v)e^{-j2\pi(f_Xu+f_Yv)}dudv}{\int I_{g,i}(u,v)dudv},
\end{equation}
and the \emph{optical transfer function} (OTF) of the system
\begin{equation}
\begin{aligned}
    \mathcal{H}(f_X,f_Y)&=\frac{\int |h(u,v)|^2e^{-j2\pi(f_Xu+f_Yv)}dudv}{\int |h(u,v)|^2dudv},\\
    &=\frac{\int H(p,q)H^*(p-f_X,q-f_Y)dpdq}{\int |H(p,q)|^2dpdq},\\
    &=\frac{\int H\left(p+\frac{f_X}{2},q+\frac{f_Y}{2}\right)H^*\left(p-\frac{f_X}{2},q-\frac{f_Y}{2}\right)dpdq}{\int |H(p,q)|^2dpdq},\\
    &=\frac{\int P\left(x+\frac{\lambda z_if_X}{2},y+\frac{\lambda z_if_Y}{2}\right)P\left(x-\frac{\lambda z_if_X}{2},y-\frac{\lambda z_if_Y}{2}\right)dxdy}{\int P(x,y)^2dxdy},\\
    &=\frac{\int_{\mathcal{A}(f_X,f_Y)}dxdy}{\int_{\mathcal{A}(0,0)}dxdy},
\end{aligned}
\end{equation}
in which the second equality uses the Parseval's theorem and the relationship between Fourier transform and autocorrelation.
\par
Interpretations:
\begin{itemize}
    \item The third equality says that \emph{the OTF is the normalized autocorrelation of the ATF}. 
    \item The last equality expresses the OTF as the ratio between the area of the overlap of two displaced pupils $\int_{\mathcal{A}(f_X,f_Y)}dxdy$ and the total area of the pupil.
\end{itemize}
\par
General properties of the OTF
\begin{itemize}
    \item $|\mathcal{H}(f_X,f_Y)| \le |\mathcal{H}(0,0)| =1$.
    \item $\mathcal{H}(-f_X,-f_Y)=\mathcal{H}^*(f_X,f_Y)$.
\end{itemize}

\subsubsection{Effects of aberrations}
\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{aberration}
\caption{Effect of aberration.}
\end{figure}

When aberrations exist, the wavefront leaving the exit pupil is deformed/distorted from the perfect sphere (i.e. the \emph{Gaussian reference sphere}). Define the effective path-length phase error $W$, the generalized pupil function is
\begin{equation}
    \mathcal{P}(x,y)=P(x,y)e^{jkW(x,y)}.
\end{equation}
For coherent systems
\begin{equation}
    H(f_X,f_Y)=\mathcal{P}(\lambda z_if_X,\lambda z_if_Y)=P(\lambda z_if_X,\lambda z_if_Y)e^{jkW(\lambda z_if_X,\lambda z_if_Y)}.
\end{equation}
For incoherent systems
\begin{equation}
    \mathcal{H}(f_X,f_Y)=\frac{\int_{\mathcal{A}(f_X,f_Y)}e^{jk\left[W\left(x+\frac{\lambda z_if_X}{2},y+\frac{\lambda z_if_Y}{2}\right)-W\left(x-\frac{\lambda z_if_X}{2},y-\frac{\lambda z_if_Y}{2}\right)\right]}dxdy}{\int_{\mathcal{A}(0,0)}dxdy},
\end{equation}
Using Schwarz's inequality, it can be proved that aberrations decrease the magnitude of the OTF, i.e. lowering the image contrast in general. The absolute cutoff frequency remains unchanged but aberrations can reduce the high frequencies to such an extent that the effective cutoff frequency is much lower than the diffraction-limited cutoff.
\par
\emph{Summary}
\begin{table}
\centering
\begin{tabular}{c|c} 
\emph{Coherent} & \emph{Incoherent} \\
\hline
$H(f_X,f_Y)=\mathcal{F}\{h\}$ & $\mathcal{H}(f_X,f_Y)=\frac{\mathcal{F}\{|h|^2\}}{|h|^2}$ \\
\hline
$I_i=|h\otimes U_g|^2$ & $I_i=|h|^2\otimes I_g=|h|^2\otimes |U_g|^2$ \\
\hline
$\mathcal{F}\{I_i\}=HG_g\star HG_g$ & $\mathcal{F}\{I_i\}=(H\star H)(G_g\star G_g)$

\end{tabular}
\caption{Comparison of coherent and incoherent responses. $\star$ is the autocorrelation.}
\end{table}

\bibliographystyle{unsrt}
\bibliography{ref}

\end{document}
