import math
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':15})

# all in nm

x=512.
y=x
dx=8.
dy=dx

nx=int(x/dx)
ny=int(y/dy)

NA=1.35
Lambda=13.
cutoff=NA/Lambda

b=np.zeros((nx,ny))

# L
#x1=64.
#x2=x1+128.
#x3=x2+256.
#y1=128.
#y2=y1+128.
#y3=y2+128.
#b[int(x1/dx):int(x3/dx),int(y1/dy):int(y2/dy)]=1
#b[int(x2/dx):int(x3/dx),int(y2/dy):int(y3/dy)]=1

x1=64.
x2=x1+384.
y1=32.
y2=y1+64.
b[int(x1/dx):int(x2/dx),int(y1/dy):int(y2/dy)]=1
b[int(x1/dx):int(x2/dx),int((y-y2)/dy):int((y-y1)/dy)]=1
b[int((x1+128.)/dx):int((x1+192.)/dx),int((y-y2)/dy):int((y-y1)/dy)]=0

x1=64.
x2=x1+384.
x3=192.
x4=x3+128.
y1=128.
y2=y1+64.
b[int(x1/dx):int(x2/dx),int(y1/dy):int(y2/dy)]=1
b[int(x1/dx):int(x2/dx),int((y-y2)/dy):int((y-y1)/dy)]=1
b[int(x3/dx):int(x4/dx),int(y2/dy):int((y-y2)/dy)]=1

x2=x1+64.
y1=y2+32.
y2=y1+64.
b[int(x1/dx):int(x2/dx),int(y1/dy):int(y2/dy)]=1

plt.figure()
plt.imshow(b,extent=(0,y,0,x))
plt.xlabel('X (nm)')
plt.ylabel('Y (nm)')
#plt.savefig('H.pdf',bbox_inches='tight')

b=np.fft.fftshift(b)
Fb=np.fft.fft2(b)
Fb=np.fft.fftshift(Fb)

fx_max=1./(2.*dx)
fy_max=1./(2.*dy)
dfx=1./x
dfy=1./y

#axs[2].imshow(np.abs(Fb),extent=(-fy_max,fy_max,-fx_max,fx_max))

P=np.zeros((nx,ny))
for i in range(nx):
    fx=(i-nx/2)*dfx
    fx2=fx*fx
    for j in range(ny):
        fy=(j-ny/2)*dfy
        fy2=fy*fy
        fxy=math.sqrt(fx2+fy2)
        if fxy<=cutoff:
            P[i,j]=1

Fb=Fb*P

#axs[3].imshow(P,extent=(-fy_max,fy_max,-fx_max,fx_max))
#axs[4].imshow(np.abs(Fb),extent=(-fy_max,fy_max,-fx_max,fx_max))

b1=np.fft.ifft2(Fb)
b1=np.fft.fftshift(b1)

plt.figure()
plt.imshow(np.abs(b1),extent=(0,y,0,x))
plt.xlabel('X (nm)')
plt.ylabel('Y (nm)')

plt.savefig('euv135.pdf',bbox_inches='tight')
plt.show()
