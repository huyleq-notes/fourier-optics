#fraunhofer.py

import math
import sys
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':15})

# all in nm

x=1024.
y=x
dx=4.
dy=dx

nx=int(x/dx)
ny=int(y/dy)

Lambda=193.
z=100.
Lambdaz=Lambda*z
Lambdaz2=Lambdaz*Lambdaz

b=np.zeros((nx,ny))

# rectangle
#w=126.
#h=126.
#sep=0.
#b[int((x-h)/2/dx):int((x-h)/2/dx+h/dx),int((y-sep-w)/2/dy):int((y-sep-w)/2/dy+w/dy)]=1
#b[int((x-h)/2/dx):int((x-h)/2/dx+h/dx),int((y+sep-w)/2/dy):int((y+sep-w)/2/dy+w/dy)]=1

# single slit
#w=4.
#h=512.
#sep=0.
#b[int((x-h)/2/dx):int((x-h)/2/dx+h/dx),int((y-sep-w)/2/dy):int((y-sep-w)/2/dy+w/dy)]=1
#b[int((x-h)/2/dx):int((x-h)/2/dx+h/dx),int((y+sep-w)/2/dy):int((y+sep-w)/2/dy+w/dy)]=1

# double slits
#w=12.
#h=512.
#sep=64.
#b[int((x-h)/2/dx):int((x-h)/2/dx+h/dx),int((y-sep-w)/2/dy):int((y-sep-w)/2/dy+w/dy)]=1
#b[int((x-h)/2/dx):int((x-h)/2/dx+h/dx),int((y+sep-w)/2/dy):int((y+sep-w)/2/dy+w/dy)]=1

# circle
r=100.
for i in range(nx):
    fx=(i-nx/2)*dx
    fx2=fx*fx
    for j in range(ny):
        fy=(j-ny/2)*dy
        fy2=fy*fy
        fxy=math.sqrt(fx2+fy2)
        if fxy<=r:
            b[i,j]=1

fig,axs=plt.subplots(1,2,figsize=(12,5))
plt.set_cmap('gray')
im=axs[0].imshow(b,extent=(-y/2,y/2,-x/2,x/2))
axs[0].set_xlabel('X (nm)')
axs[0].set_ylabel('Y (nm)')

b1=np.fft.fftshift(b)
Fb=np.fft.fft2(b1)
Fb=np.fft.fftshift(Fb)

fx_max=1./(2.*dx)
fy_max=1./(2.*dy)
dfx=1./x
dfy=1./y

dxi=Lambda*z*dfx
dyi=Lambda*z*dfy
xi_max=dxi*nx
yi_max=dyi*ny

im=axs[1].imshow(np.abs(Fb)/Lambdaz2,extent=(-yi_max/2,yi_max/2,-xi_max/2,xi_max/2))
axs[1].set_xlim(-y/2,y/2)
axs[1].set_ylim(-x/2,x/2)
axs[1].set_xlabel('X (nm)')
axs[1].set_ylabel('Y (nm)')

plt.savefig('circle.pdf',bbox_inches='tight')
plt.show()
